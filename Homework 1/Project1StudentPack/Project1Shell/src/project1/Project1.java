/**
 * @author Tim Steele
 * CSC 210
 */
package project1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Project1
{
    public static void main(String[] args) throws FileNotFoundException
    {
       JFileChooser  chooser = new JFileChooser();
       int result = chooser.showOpenDialog(null);
       List<Kid> kids = new ArrayList<>();
       Map<Integer, List<String>> ageGroups = new HashMap<>();
       
       
       
       if (result != JFileChooser.APPROVE_OPTION)
       {
           JOptionPane.showMessageDialog(null, "No file selected.", 
                    "Kid Analytics Program",
                    JOptionPane.ERROR_MESSAGE);
           return ;
       }
       
       // Open the file with a buffered reader
       FileReader fReader = new FileReader(chooser.getSelectedFile());
       BufferedReader bReader = new BufferedReader(fReader);
       
       // Get a Stream<String> of the lines from the buffered teader
       Stream<String> lines = bReader.lines();

       // Get each line, parse the kid, and add to Array/Map;
       lines.forEach(x -> {
           
           (parseKid(x)).ifPresent(
               y -> {
                   addToArrayAndMap(y, kids, ageGroups);
               }
           ); 
       });
       
       // Print normal;
       printKidList("Here is the list in original order:", kids);
       
       Collections.sort(kids,(kid1, kid2) -> {
           return kid1.name.compareTo(kid2.name);
       });
       
       // Print alphabetical;
       printKidList("Here is the list in alphabetical order:", kids);
       
       Collections.sort(kids, (kid1, kid2) -> {
           if (kid1.age > kid2.age) return -1;
           else if (kid1.age < kid2.age) return 1;
           else return 0;
       });
       
       // Print by age;
       printKidList("Here is the list in descending order of age:", kids);
       
       System.out.println("\nHere are the age groups:");
       ageGroups.forEach((age,group) -> {
            System.out.print(age + " : " + group + "\n");
       });
        
    }
    
    private static Optional<Kid> parseKid(String kid) {
        if (!"".equals(kid)) {
            //return
            String[] kidsInfo = kid.split(" +");
            
            // Make sure we have a name and something;
            if (kidsInfo.length == 2) {
                
                // Make sure "age" is an integer;
                try {
                    int age = Integer.parseInt(kidsInfo[1]);
                  
                    // Age didn't fail, we have a name and age;
                    // Make a kid and return him;
                    return Optional.of(new Kid(kidsInfo[0], age));
                    
                } catch(NumberFormatException e) {
                    // age isn't an integer
                    // Fail silently and return empty;
                    return Optional.empty();
                }
                
            } else {
                // Don't have name or age, return empty;
                // We could spit out an error or something
                // but I am feeling lazy...;
                return Optional.empty();
            }
            
        } else {
            // Empty line, no kid;
            return Optional.empty();
        }
    }
    
    private static void addToArrayAndMap(Kid kid, List<Kid> kids, Map<Integer, List<String>> ages) {
        // Make sure no one crashes the app by passing us incomplete args;
        if (kid != null && kids != null && ages != null) {
            kids.add(kid);
            
            // Not sure if this is a performance issue in Java
            // but going to be using the age a lot so cache the ref;
            int a = kid.age;

            // Make sure we gots keyz otherwise make 'em;
            if (ages.get(a) == null) ages.put(a, new ArrayList<>());
            
            // Add the kid to age group;
            ages.get(a).add(kid.name);
            
        }
    }
    
    private static void printKidList(String message, List<Kid> kids) {
        // This is to avoid having to type a bunch of print lines;
        System.out.println(message);
        kids.forEach((kid)-> {
            System.out.println(kid.name + " " + kid.age);
        });
        System.out.println("");
    }
    
}

class Kid
{
   String name;
   int age;
   public Kid(String name, int age)
   {
       this.name = name;
       this.age = age;
   }
   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append(name).append(" ").append(age);
      return builder.toString();
   }
}



