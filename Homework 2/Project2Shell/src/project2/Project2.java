
package project2;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

interface AvailableCmd
{
     String [ ] cmds1 = {"add string", "add index string",  "remove string",
                           "removeAll string", "remove index", };

     String [ ] cmds2 = {"size", "get index",  "set index string",
                           "indexOf string",  "contains string",
                           "count string", 
                           };

     String [ ] cmds3 = {"uppercase index", "lowercase index",  "reverse",
                           "addList string ...",   };
}

public class Project2 extends Application
{    
    @Override
    public void start(Stage stage)
    {
       
        VBox topLevelPane = new VBox(10);
        topLevelPane.setAlignment(Pos.CENTER);        
        topLevelPane.setPadding(new Insets(10));
        
        HBox cmdListPane = new HBox(10);
        cmdListPane.setAlignment(Pos.CENTER);
        cmdListPane.getChildren().addAll
            (
                new CmdListTextArea(AvailableCmd.cmds1),  
                new CmdListTextArea(AvailableCmd.cmds2),  
                new CmdListTextArea(AvailableCmd.cmds3)  
            );
        
        Label availableCmdsLabel = new Label("Available Commands");
        topLevelPane.getChildren().addAll
            (
              availableCmdsLabel,      
              cmdListPane
            );       
        
        // List View
        Label listViewLabel  = new Label("List View");
        TextArea listViewTextArea = new TextArea();
        listViewTextArea.setEditable(false);
        listViewTextArea.setPrefRowCount(30);
        listViewTextArea.setPrefColumnCount(60);
        ScrollPane scrollPane = new ScrollPane();     
        scrollPane.setPrefViewportHeight(300);       
        scrollPane.setContent(listViewTextArea);
        
        topLevelPane.getChildren().addAll
            (
              listViewLabel,      
              scrollPane
            );
        
        // Command Entry and Result Text Fields
        TextField cmdEntryTextField = new TextField();
        TextField cmdResultTextField = new TextField();
        cmdResultTextField.setEditable(false);
        GridPane cmdGridPane = new GridPane();
        cmdGridPane.setHgap(10);
        cmdGridPane.setVgap(10);
        cmdGridPane.setAlignment(Pos.CENTER);
        cmdGridPane.add(new Label("Command"), 0, 0);
        cmdGridPane.add(new Label("Command Result"), 1, 0);
        cmdGridPane.add(cmdEntryTextField, 0, 1);
        cmdGridPane.add(cmdResultTextField, 1, 1);
        
        topLevelPane.getChildren().addAll(cmdGridPane);      
        
        Scene scene = new Scene(topLevelPane);
        
        stage.setTitle("CSC 210 Linked List Exercises");
        stage.setScene(scene);
        stage.show();
        
        
        // Create the list
        CSC210List ourList = new CSC210List();
        // Create the Command Interpreter
        EventHandler<ActionEvent> cmdInterpeter =
                new CommandInterpreter(cmdResultTextField, 
                                       ourList, listViewTextArea);
        cmdEntryTextField.setOnAction(cmdInterpeter);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
    
}

// This class displays a list of available commands
class CmdListTextArea extends TextArea
{
    public CmdListTextArea(String [] cmds)
    {
        this.setEditable(false);
        this.setPrefColumnCount(10);
        this.setPrefRowCount(6);
        for (String s : cmds)
        {
            this.appendText(s + "\n");
        }
    }
}


class CSC210List
{
    private static class Node
    {
        String value;
        Node next;
        Node(String value)
        {
           this(value, null);
        }
        Node(String value, Node next)
        {
           this.value = value;
           this.next = next;
        }
    }

    private Node first;

    // public methods
   
    /**
     * Add the value at the end of this list
     * @param value
     */
  
     public void add(String value)
    {
        if (first == null)
        {
            first = new Node(value, first);
            return;
        }
        // find the last node and add the value after it
        Node lastPointer = first;  // will be used to point to the last node
        while (lastPointer.next != null)
        {
            lastPointer = lastPointer.next;
        }
        // found the last node, add the value after it
        lastPointer.next = new Node(value);
    }
    /**
     * add the value to this list at the given position
     * @param index
     * @param value
     */
    public void add(int index, String value)
    {
       // Check to see if we are just setting the first;
       if (index == 0 || first == null) first = new Node(value, first);
       
       else {
            int count = 1;
            Node next = first.next;
            Node current = first; 
            
           // Walk through, find our current and keep
           // track of our previous;
           while (next != null && count < index) {
               count++;

               current = next;
               next = current.next;
           }

            Node newNode = new Node(value, current.next);
            current.next = newNode;
       }
    }

    /**
     * Add the array of values in order to the end of this list
     * @param values
     */
    public void addList(String [] values)
    {   
        // Stream through and add;
        Stream<String> vals = Arrays.stream(values);
        vals.forEach(x -> {
            this.add(x);
        });
    }
    /**
     *
     * @param value
     * @return true if this list contains the value; false otherwise
     */
    public boolean contains(String value)
    {
        // If it is empty, it doesn't have the value;
        if (first == null) return false;
        
        // Check against the first;
        if (value.equals(first.value)) return true;
        
        // Check against the rest;
        Node stepper = first.next;
        while(stepper != null) {
            if (value.equals(stepper.value)) return true;
            stepper = stepper.next;
        }
        
        // Didn't find it;
        return false;
    }
    /**
     *
     * @param value
     * @return number of times the given value appears in this list
     */
    public int count(String value)
    {
       // If it is empty, you won't find the value;
       if (first == null) return 0;
       
       Node stepper = first;
       int count = 0;
       
       // Loop through and count;
       while(stepper != null) {
           if (value.equals(stepper.value)) count++;
           stepper = stepper.next;
       }
       
       return count;
    }
    /**
     *
     * @param index
     * @return the string at the position given by index
     */
    public String get(int index)
    {
        // Make sure we aren't empty and don't get out of bounds;
        if (first == null || index >= this.size()) return null;
        
        // Check against the first;
        if (index == 0) return first.value;
        
        int count = 0;
        Node stepper = first;
        
        while (count < index) {
            count++;
            stepper = stepper.next;
        }
        
        // Return the value;
        return stepper.value;
    }
    /**
     *
     * @param value
     * @return the position of the given value in this string, or -1 if the
     * value is not found
     */
    
    // This should technically be firstIndexOf() as we aren't forcing unqiue;
    public int indexOf(String value)
    {
        Node stepper = first;
        int index = 0;
        while(stepper != null) {
            if (value.equals(stepper.value)) return index;
            index++;
            stepper = stepper.next;
        }
        
        // Didn't find it;
        return -1;
    }

    /**
     * Converts to lowercase the string at the given position
     * @param index
     */
    public void lowerCase(int index)
    {       
       Node stepper = first;
       int count = 0;
       while(stepper != null && index != count) {
           count++;
           stepper= stepper.next;
       }
       
       // We have the right number of nodes;
       if (index == count) stepper.value = stepper.value.toLowerCase();
    }
    /**
     * remove the first occurrence of the specified string from this list
     * @param value
     * @return true if removed, false if the list does not contain the value.
     */
    public boolean remove(String value)
    {
        // Get it's index;
        int index = this.indexOf(value);
        
        // Check if it exists;
        if (index != -1) {
            if (index == 0) {
                first = first.next;
                return true;
            }
            
            Node next = first.next;
            Node current = first;
            int count = 1;
            while (count != index) {
                current = next;
                next = current.next;
                count++;
            }
            
            current.next = next.next;
            
            return true;
            
        } else return false;
    }
    /***
     * Remove all occurrences of this string from the list
     * @param value
     */
    void removeAll(String value)
    {
        // While the value still exists, keep removing;
        while(this.indexOf(value) != -1) {
            this.remove(value);
        }
    }
    /**
     * Remove the string at the given position from this list
     * @param index
     * @return
     */
    String remove(int index)
    {
       if (first != null && this.size() > index) {
           Node deleted;
           
           if (index == 0) {
               deleted = first;
               first = first.next;
               return deleted.value;
           }
           
           Node next = first.next;
           Node current = first;
           int count = 1;
           
           while (count != index) {
               current = next;
               next = current.next;
               count++;
           }
          
           deleted = current.next;
           
           current.next = next.next;
           return deleted.value;
       } else return null;
        
    }
    /**
     * Reverse the order of the elements in this list
     */
    void reverse()
    {
      String[] values = new String[this.size()];
      int index = this.size()-1;
      while(first != null) {
          values[index] = this.remove(0);
          index--;
      }

      this.addList(values);
    }
    /**
     * Set a new value for this list at the given position
     * @param index
     * @param value
     * @return the old value at that position
     */
    public String set(int index, String value)
    {
        if (first == null || this.size() < index) return null;
        
        Node stepper = first;
        int count = 0;
        while(count != index) {
            stepper = stepper.next;
            count++;
        }
        
        String oldValue = stepper.value;
        stepper.value = value;
        
        return oldValue;
    }
    /**
     *
     * @return size of this list
     */
    public int size()
    {
        int count = 0;
        Node stepper = first;
        while (stepper != null)
        {
            count ++;
            stepper = stepper.next;
        }
        return count;
    }
    /**
     * transforms to uppercase the string at the given position in this list
     * @param index
     */
    public void upperCase(int index)
    {       
       Node stepper = first;
       int count = 0;
       while(stepper != null && index != count) {
           count++;
           stepper= stepper.next;
       }
       
       // We have the right number of nodes;
       if (index == count) stepper.value = stepper.value.toUpperCase();
    }

    /**
     *
     * @return String form of contents of list
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("[");
        Node stepThru = first; // Used to step through the list
        while (stepThru != null)
        {
            sb.append(stepThru.value);
            stepThru = stepThru.next;
            if (stepThru != null) // are there more?
               sb.append(", ");
        }
        sb.append("]");
        return sb.toString();
    }
}


/**
 * This will be used to listen for ActionEvents on a text fields.
 * When invoked, it will get the command from the text field (getText())
 * and interpret it on the list object. It will then redisplay the resulting list
 * in a TextArea passed as constructor parameter. If the list method
 * returns a value, the value is displayed in the resultTextfield, also passed
 * as parameter.
 *
 * @author Godfrey Muganda
 */
class CommandInterpreter implements EventHandler<ActionEvent> 
{
    TextField resultTextField;
    TextArea listViewTextArea;
    CSC210List myList;
    
    
    public CommandInterpreter(TextField resultText, 
                              CSC210List myList,
                              TextArea listViewTextArea)
    {        
        this.resultTextField = resultText;
        this.myList = myList;
        this.listViewTextArea = listViewTextArea;
    }
    
     @Override
    public void handle(ActionEvent event)
    {       

       // Get the command typed by the user and put a Scanner on it
       TextField cmdTextField = (TextField) event.getTarget();
       String input = cmdTextField.getText();
       Scanner scanner = new Scanner(input);

       // Check to see what method is being called and call it on
       // the list
       String methodName = scanner.next();
       if (methodName.equalsIgnoreCase("add"))
       {
           // Must be add index value or add value
           int index;
           String value;
           if (scanner.hasNextInt())
           {
              // add index value: get the parameters
              index = scanner.nextInt();
              value = scanner.next();
              // call the method
              myList.add(index, value);
              // redisplay the list in the frame
           }
           else
           {
              // add value: get the parameter
              value = scanner.next();
              // call the method
              myList.add(value);

           }
           // Redisplay the list in the frame
           listViewTextArea.appendText(myList.toString()+ "\n");
           return;
       }
       // addList
       // Hint: Use the string split(" ") method to create an array
       // to pass to the list method addList
       if (methodName.equalsIgnoreCase("addList"))
       {
          String [] values = scanner.nextLine().trim().split(" ");           
          myList.addList(values);
          listViewTextArea.appendText(myList.toString()+ "\n");       
          return;
       }
       // contains
       if (methodName.equalsIgnoreCase("contains"))
       {
           String value = scanner.next();
           resultTextField.setText(String.valueOf(myList.contains(value)));
           return;
       }
       if (methodName.equalsIgnoreCase("count"))
       {
           String value = scanner.next();
           resultTextField.setText(String.valueOf(myList.count(value)));
           return;
       }
       // get index
       if (methodName.equalsIgnoreCase("get"))
       {
          int index = scanner.nextInt();
           resultTextField.setText(String.valueOf(myList.get(index)));
           return;
       }
       // indexOf value
       if (methodName.equalsIgnoreCase("indexOf"))
       {
           String value = scanner.next();
           resultTextField.setText(String.valueOf(myList.indexOf(value)));
           return;
       }
       // lowercase index
       if (methodName.equalsIgnoreCase("lowerCase"))
       {
          int index = scanner.nextInt();
          myList.lowerCase(index);
            listViewTextArea.appendText(myList.toString()+ "\n");
          return;
       }
       // remove value or remove index
       if (methodName.equalsIgnoreCase("remove"))
       {
           if (scanner.hasNextInt())
           {
               // remove index
                int index = scanner.nextInt();
                String removedValue = myList.remove(index);
                resultTextField.setText(removedValue);
           }
          else
          {
               // remove value
               String value = scanner.next();
               resultTextField.setText(String.valueOf(myList.remove(value)));
          }
            listViewTextArea.appendText(myList.toString()+ "\n");
          return;
       }
       //removeAll value
       if (methodName.equalsIgnoreCase("removeAll"))
       {
           String value = scanner.next();
           myList.removeAll(value);
             listViewTextArea.appendText(myList.toString()+ "\n");
       }
       // reverse
       if (methodName.equalsIgnoreCase("reverse"))
       {
           myList.reverse();
             listViewTextArea.appendText(myList.toString()+ "\n");
           return;
       }
       // set index value
       if (methodName.equalsIgnoreCase("set"))
       {
          int index = scanner.nextInt();
          String value = scanner.next();
          String oldValue = myList.set(index, value);
            listViewTextArea.appendText(myList.toString()+ "\n");
          resultTextField.setText(String.valueOf(oldValue));
          return;
       }
        // size
       if (methodName.equalsIgnoreCase("size"))
       {
           int result = myList.size();
           // get the result and put it in the result text field
           resultTextField.setText(String.valueOf(result));
       }
       // uppercase index
       if (methodName.equalsIgnoreCase("upperCase"))
       {
          int index = scanner.nextInt();
          myList.upperCase(index);
            listViewTextArea.appendText(myList.toString()+ "\n");
       }
    }  

}
