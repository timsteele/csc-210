/**
 * @author Tim Steele, Dr. Muganda
 */

package BinaryTrees;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Godfrey
 */
class BinaryTree
{
    public JPanel getView()
    {
        return new BinaryTreeView(root);
    }
    static class Node
    {
        int value;
        Node left, right;
        Node(int value)
        {
           this(value, null, null);
        }
        Node(int value, Node left, Node right)
        {
           this.value = value;
           this.left = left; 
           this.right = right;
        }
    }
    
    private static class RemovalResult
    {
        Node removedNode;  // set to null if nothing was removed
        Node remainingTree;
       
        RemovalResult(Node removedNode, Node rT)
        {
            this.removedNode = removedNode;
            this.remainingTree = rT;           
        }
    }

    private Node root;
    /**
     * clears all values and empties the binary tree
     */
    public void clear()
    {
        root = null;
    }
    
    /**
     * Return size of this binary tree object
     * @return 
     */
    public int size()
    {
        return size(root);
    } 
    
     /**
     * Return number of nodes in a binary tree
     * @param r : root of the binary tree
     * @return 
     */
    private static int size(Node root)
    {
        if (root == null) return 0;
        
        int count = 0;
        
        if (root.left != null) count += size(root.left);
        if (root.right != null) count += size(root.right);
        
        return count + 1;
    }
    /**
     * Adds a value to this binary tree object
     * @param value 
     */
    public void add(int value)
    {
        root = add(root, value);
    }
    
       /**
     * Adds a value to the binary tree with the given root
     * @param root
     * @param value
     * @return a pointer to the root of the augmented binary tree
     */
    private static Node add(Node root, int value)
    {
         if (root == null) 
         {
             return new Node(value);
         }
         if (value < root.value)
             root.left = add(root.left, value);
         else
             root.right = add(root.right, value);
         return root;
    } 
    
    /**
     * 
     * @return a preorder list of the values in the binary tree
     */
    public List<Integer> preorder()
    {
        // Create empty list to be filled in by the recursive method
        List<Integer> preorderList = new ArrayList<>();
        preorder(root, preorderList);
        return  preorderList;
    }
    /**
     * Adds all values in nodes rooted at root to the preorderList
     * @param root
     * @param preorderList 
     */
   private static void preorder(Node root, List<Integer> preorderList)
   {
        if (root == null) return;
        preorderList.add(root.value);
        
        preorder(root.left, preorderList);
        preorder(root.right, preorderList);
        
   }
   
      /**
     * 
     * @return a postorder list of the values in the binary tree
     */
    public List<Integer> postorder()
    {
        // Create empty list to be filled in by the recursive method
        List<Integer> postorderList = new ArrayList<>();
        postorder(root, postorderList);
        return  postorderList;
    }
    /**
     * Adds all values in nodes rooted at root to the postorderList
     * @param root
     * @param postorderList 
     */
   private static void postorder(Node root, List<Integer> postorderList)
   {
       if (root == null) return;
       
       postorder(root.left, postorderList);
       postorder(root.right, postorderList);
       
       postorderList.add(root.value);
   }
 
    /**
     * 
     * @return a inorder list of the values in the binary tree
     */
    public List<Integer> inorder()
    {
        // Create empty list to be filled in by the recursive method
        List<Integer> inorderList = new ArrayList<>();
        inorder(root, inorderList);
        return  inorderList;
    }
    /**
     * Adds all values in nodes rooted at root to the inorderList
     * @param root
     * @param inorderList 
     */
   private static void inorder(Node root, List<Integer> inorderList)
   {
       if (root == null) return;
       
       inorder(root.left, inorderList);
       inorderList.add(root.value);
       inorder(root.right, inorderList);
   }
   /**
    * 
    * @return height of the binary tree
    */
   public int treeHeight()
   {
       return treeHeight(root);
   }
   /**
    * 
    * @param root
    * @return height of the binary tree with the given root
    */
   private static int treeHeight(Node root)
   {
       if (root == null) return -1;
       if (root.left == null && root.right == null) return 0;
       
       int left = treeHeight(root.left);
       int right = treeHeight(root.right);
       
       if (left > right) return left + 1;
       else return right + 1;
   }
   /**
    * 
    * @return the list of all leaves of the binary tree.
    */
   public List<Integer> leaves()
   {      
      List<Integer> listOfLeaves = new ArrayList<>();
      leaves(root, listOfLeaves); 
      return listOfLeaves;
   }
   /**
    * Add all leaves in the binary tree rooted at root into a list  listOfLeaves
    * @param root
    * @param listOfLeaves 
    */
   private static void leaves(Node root, List<Integer> listOfLeaves)
   {
       if (root == null) return;
       if (root.left == null && root.right == null) listOfLeaves.add(root.value);
       else {
           leaves(root.left, listOfLeaves);
           leaves(root.right, listOfLeaves);
       }
   }
   /**
    * 
    * @param level
    * @return list of all values in the binary tree at the given level
    */
   public List<Integer> atLevel(int level)
   {
       List<Integer> valuesAtLevel = new ArrayList<>();
       atLevel(root, valuesAtLevel, level);
       return valuesAtLevel;
   }
   /**
    * Adds all values at the given level in the binary tree rooted at root to 
    * the list valuesAtLevel
    * @param root
    * @param valuesAtLevel
    * @param level 
    */
   private static void atLevel(Node root, List<Integer> valuesAtLevel, int level)
   {
       if (root == null) return;
       if (level == 0) valuesAtLevel.add(root.value);
       else {
           atLevel(root.left, valuesAtLevel, level-1);
           atLevel(root.right, valuesAtLevel, level-1);
       }
   }
   /**
    * remove a value from the binary tree
    * @param value
    * @return true if the value was removed, false otherwise
    */
   public  boolean remove(int value)
   {
      RemovalResult res = remove(root, value);
      root = res.remainingTree;
      return res.removedNode != null;    
   }
   
   private static RemovalResult remove(Node root, int value)
   {
       if (root == null) return new RemovalResult(null, null);
       
       // Is value to be removed in the root?
       if (root.value == value)
       {
           // is there no left subtree?
           if (root.left == null)
           {
               RemovalResult res = new RemovalResult(root, root.right);
               res.removedNode.left = null;
               res.removedNode.right = null;
               return res;
           }
           // Is there no right subtree?
           if (root.right == null)
           {
               RemovalResult res = new RemovalResult(root, root.left);
               res.removedNode.left = null;
               res.removedNode.right = null;
               return res;
           }
           // The node to be removed has two children
           // remove the node and replace it with the leftmost node in the 
           // right subtree
           
           // First remove the leftmost node in the right subtree 
           RemovalResult res = removeLeftMost(root.right);
           // Make the removed node the root of what will be the remaining tree
           res.removedNode.left = root.left;
           res.removedNode.right = res.remainingTree;
           // Make the removed node the remaining tree
           res.remainingTree = res.removedNode;
           // Make the original root the removed node
           res.removedNode = root;
           root.left = null;
           root.right = null;
           return res;          
       }       
       // This is the part where the value to be removed is not in the root
       if (value < root.value)
       {
           RemovalResult res = remove(root.left, value);
           root.left = res.remainingTree;
           res.remainingTree = root;
           return res;
       }       
       if (value > root.value)
       {
           RemovalResult res = remove(root.right, value);
           root.right = res.remainingTree;
           res.remainingTree = root;
           return res;
       }      
       return null;
   }
   /**
    * remove and return the value in the right most node
    * @return 
    */
   public int removeRightMost() 
   {
      if (root == null) throw new IllegalStateException("The binary tree is empty!");       
      RemovalResult res = removeRightMost(root);
      return res.removedNode.value;
   }
   /**
    * Remove the rightmost node in the tree rooted at the given root
    * @param root
    * @return 
    */
   private static RemovalResult removeRightMost(Node root)
   {
       if (root == null) throw new IllegalStateException("The binary tree is empty!");
       // Is the root the rightmost node?
       if (root.right == null)
       {
           RemovalResult res = new RemovalResult(root, root.left);
           res.removedNode.left = null;
           return res;
       }
       // There are nodes to the right of the root       
       Node probeRight = root;
       while (probeRight.right.right != null)
       {
           probeRight= probeRight.right;
       }
       // probeRight.right.right == null;
       // probeRight.right needs to be taken out
       RemovalResult res = new RemovalResult(probeRight.right, root);
       // take out the probeRight.right.right node
       probeRight.right = probeRight.right.left;
       res.removedNode.left = null;
       res.removedNode.right = null;
       return res;
   }
   
   /**
    *  
    * @param value
    * @return level at which a value is found, or -1 if the value is not in the tree
    */
   public int levelOf(int value)
   {
      return levelOf(root, value); 
   }
   /**
    * 
    * @param root
    * @param value
    * @return level of the value in the binary tree rooted at the given root.
    */
   private static int levelOf(Node root, int value)
   {
       List<Integer> values, valuesAtLevel;
       if (root == null) return -1;
       
       int height = treeHeight(root);
       
       while(height > -1) {
           valuesAtLevel = new ArrayList<>();
           atLevel(root, valuesAtLevel, height);
           if (valuesAtLevel.contains(value)) return height;
           height--;
       }
       
       return -1;
       
   }
   
    /**
    * remove and return the value in the left most node
    * @return 
    */
   public int removeLeftMost() 
   {
      if (root == null) throw new IllegalStateException("The binary tree is empty!");
       
      RemovalResult res =  removeLeftMost(root);  
      root = res.remainingTree;
      return res.removedNode.value;
   }
   /**
    * Remove the rightmost node in the tree rooted at the given root
    * @param root
    * @return 
    */
   private static RemovalResult removeLeftMost(Node root)
   {
       if (root == null) throw new IllegalStateException("The binary tree is empty!");
       // Is the root the leftmost node?
       if (root.left == null)
       {
           RemovalResult res = new RemovalResult(root, root.right);
           res.removedNode.right = null;
           return res;
       }
       // There are nodes to the left of the root       
       Node probeLeft = root;
       while (probeLeft.left.left != null)
       {
           probeLeft= probeLeft.left;
       }
       // probeLeft.left.left == null;
       // probeLeft.left needs to be taken out
       RemovalResult res = new RemovalResult(probeLeft.left, root);
       // take out the probeLeft.left.left node
       probeLeft.left = probeLeft.left.right;
       res.removedNode.left = null;
       res.removedNode.right = null;
       return res;
   }
   /**
    * Does not have to be recursive
    * @return the value in the leftmost node in the binary tree
    */
   public int getLeftMost()
   {
      if (root == null) throw new IllegalStateException("Binary tree is empty.");
      Node stepper = root;
      while(stepper.left != null) {
          stepper = stepper.left;
      }
      
      return stepper.value;
   }
   
   
   /**
    * Does not have to recursive
    * @return the value in the rightmost node in the binary tree
    */
   public int getRightMost()
   {
      if (root == null) throw new IllegalStateException("Binary tree is empty.");
      Node stepper = root;
      while(stepper.right != null) {
          stepper = stepper.right;
      }
      
      return stepper.value;
   } 
}

