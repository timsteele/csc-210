package BinaryTrees;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
public class Main
{
    public static void main(String[] args)
    {
       new MyFrame();
    }
}

class MyFrame extends JFrame
{
   JTextArea  listDisplayTextArea; // used to display the  current contents
                                    // of the list.
   private JComponent view = new JPanel();
   BinaryTree myTree = new BinaryTree();

   
   public MyFrame()
    {
      super("Binary Tree Exercises");      

       String [ ] cmds1 = {"add value",  "randomAdd count", "remove value", 
                           "removeRightMost", "removeLeftMost",                          
                          };

       String [ ] cmds2 = {"size", "treeHeight", "preorder", "inorder",  "postorder", };

       String [ ] cmds3 = {"levelOf value", "atLevel int", "leaves", "getRightMost", 
                            "getLeftMost", "clear",
                          };

       JPanel cmdPanel = new JPanel(new GridLayout(1, 3, 20, 20));
      
       cmdPanel.add(new JLabel(toHTML(cmds1)));
       cmdPanel.add(new JLabel(toHTML(cmds2)));
       cmdPanel.add(new JLabel(toHTML(cmds3)));
       JPanel cmdPanel1 = new BorderedPanel("Available Commands");
       
       cmdPanel1.add(cmdPanel);
     
       JPanel outerNorthPanel = new JPanel();
       outerNorthPanel.add(cmdPanel1);
       this.add(outerNorthPanel, BorderLayout.NORTH);
       // create the textArea for displaying results

       listDisplayTextArea = new JTextArea(5, 60);
       listDisplayTextArea.setEditable(false);
       JScrollPane scroller = new JScrollPane(listDisplayTextArea);
       scroller.setBorder(new EmptyBorder(20, 20, 20, 20));
      
       // Place for the user to enter commands
       JTextField cmdTextField = new JTextField(20);
       JTextField cmdResultTextField = new JTextField(10);
       cmdResultTextField.setEditable(false);
       cmdResultTextField.setBackground(Color.PINK);
       BorderedPanel cmdBorderedPanel = new BorderedPanel("Command ");
       cmdBorderedPanel.add(cmdTextField, BorderLayout.NORTH);

       BorderedPanel resultBorderedPanel = new BorderedPanel("Command Result ");
       resultBorderedPanel.add(cmdResultTextField, BorderLayout.NORTH);

       JPanel userInputPanel = new JPanel(new BorderLayout());
       userInputPanel.setBorder(new EmptyBorder(20, 20, 20, 20));

       userInputPanel.add(cmdBorderedPanel, BorderLayout.NORTH);
       userInputPanel.add(scroller);

       this.add(view);  //Initially adds an empty panel as the center component
       this.add(userInputPanel, BorderLayout.SOUTH);

       pack();
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       // create the binary tree

       // set the command listener on the text field
       CommandInterpreter listener = new CommandInterpreter();
       listener.putValue("FRAME", this);
       listener.putValue("CMD", cmdTextField);
       listener.putValue("RESULT", cmdResultTextField);
       listener.putValue("BTREE", myTree);
       cmdTextField.addActionListener(listener);

       setVisible(true);
    }

   static String toHTML(String [] input)
    {
       StringBuilder htmlBuilder = new StringBuilder();
       htmlBuilder.append("<html> ");
       for (String s : input)
           htmlBuilder.append(s).append("<br />");
       htmlBuilder.append("</html>");
       return htmlBuilder.toString();
    }

   public void setCmdResults(String str)
    {
       this.listDisplayTextArea.append(str + "\n");
    }
   
   public void resetView()
   {
       // remove the current view
       this.remove(this.view);
       //  replace the view, first saving it for later removal
       this.view = myTree.getView();
       this.add(view);
       this.pack();
   }
}

class BorderedPanel extends JPanel
{
    public BorderedPanel(String title)
    {
        Border lineBorder =  BorderFactory.createLineBorder(new Color(150, 220, 250));
        Border tBorder = BorderFactory.createTitledBorder(lineBorder, title);
        setBorder(tBorder);     
    }
     @Override
    public Insets getInsets()
    {
       return new Insets(20, 20, 20, 20);
    }
}



