package BinaryTrees;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.swing.AbstractAction;
import javax.swing.JTextField;


/**
 * This will be used to listen for ActionEvents on a text fields.
 * When invoked, it will get the command from the text field (getText())
 * and interpret it on the binary tree object. It will then redisplay the resulting
 * binary tree.
 * in the center of the BorderLayout of the frame. If the binary tree method
 * returns a value, the value is displayed in the result text field.
 *
 * The interpreter uses keys "FRAME", "CMD", "BTREE", "RESULT" for
 * the frame, command text field, the binary tree object, and the result text field,
 * respectively.
 * @author Godfrey Muganda
 */
class CommandInterpreter extends AbstractAction
{
    Random randy = new Random();

    public void actionPerformed(ActionEvent e)
    {
       JTextField cmdTextField = (JTextField)this.getValue("CMD");
       JTextField resultTextField = (JTextField)this.getValue("RESULT");
       BinaryTree myTree = (BinaryTree)this.getValue("BTREE");
       MyFrame myFrame = (MyFrame)this.getValue("FRAME");

       // Get the command typed by the user and put a Scanner on it
       String input = cmdTextField.getText();
       Scanner scanner = new Scanner(input);

       String method = scanner.next();
       switch(method.toLowerCase())
       {
           case "size":
               int number = myTree.size();
               myFrame.setCmdResults(String.valueOf(number));
               return;
           case "add":
               int value = scanner.nextInt();
               myTree.add(value);
               myFrame.resetView();
               return;
           case "preorder":  
               List<Integer> preorderList = myTree.preorder();
               myFrame.setCmdResults(preorderList.toString());
               return;
           case "postorder":  
               List<Integer> postorderList = myTree.postorder();
               myFrame.setCmdResults(postorderList.toString());
               return;
           case "inorder":  
               List<Integer> inorderList = myTree.inorder();
               myFrame.setCmdResults(inorderList.toString());
               return;
           case "randomadd":  
               int count = scanner.nextInt();
               while (count >=1)
               {
                   myTree.add(randy.nextInt(200));
                   myFrame.resetView();
                   count --;
               }
              return;
           case "clear":
              myTree.clear();
              myFrame.resetView();
              return; 
           case "treeheight":
               int height = myTree.treeHeight();
               myFrame.setCmdResults(String.valueOf(height));
               return;
           case "leaves":
               List<Integer> listOfLeaves = myTree.leaves();
               myFrame.setCmdResults(String.valueOf(listOfLeaves));
               return;
           case "atlevel":
               int level = scanner.nextInt();
               List<Integer> atLevelList = myTree.atLevel(level);
               myFrame.setCmdResults(String.valueOf(atLevelList));
               return;  
           case "removerightmost":
               if (myTree.size() == 0)
               {
                   myFrame.setCmdResults("The binary tree is empty.");
                   return;
               }
               value = myTree.removeRightMost();
               myFrame.resetView();
               myFrame.setCmdResults(String.valueOf(value));
               return;
           case "removeleftmost":
               if (myTree.size() == 0)
               {
                   myFrame.setCmdResults("The binary tree is empty.");
                   return;
               }
               value = myTree.removeLeftMost();
               myFrame.resetView();
               myFrame.setCmdResults(String.valueOf(value));
               return;
           case "remove":
               value = scanner.nextInt();
               boolean flag = myTree.remove(value);               
               myFrame.resetView();
               myFrame.setCmdResults(String.valueOf(flag));
               return;               
           case "levelof":
               value = scanner.nextInt();
               level = myTree.levelOf(value);
               myFrame.setCmdResults(String.valueOf(level));
               return;
           case "getleftmost":
               value = myTree.getLeftMost();
               myFrame.setCmdResults(String.valueOf(value));
               return;
           case "getrightmost" :
               value = myTree.getRightMost();
               myFrame.setCmdResults(String.valueOf(value));
               return;
               
               
               
       }
     
    
      
      
     
   
    }
}


