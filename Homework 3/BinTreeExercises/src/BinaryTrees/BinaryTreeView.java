
 
package BinaryTrees;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BinaryTreeView extends JPanel
{
   BinaryTreeView(BinaryTree.Node root )
    {
        if (root == null) return;
        setLayout(new BorderLayout());
        // put root in North of the border layout
        JPanel rootPanel = new JPanel();
        JTextField rootTextField = new JTextField(String.valueOf(root.value));
        rootTextField.setEditable(false);
        rootTextField.setBackground(Color.yellow);
        rootPanel.add(rootTextField);  
        this.add(rootPanel, BorderLayout.NORTH);
        
        this.add(new BinaryTreeView(root.left), BorderLayout.WEST);
        this.add(new BinaryTreeView(root.right), BorderLayout.EAST);       
    }
    
}
