
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.stream.IntStream;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tim
 */
public class GraphSearch
{    
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "Graph Data", "txt", "grf");

        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(null);

        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
            System.out.println("No file selected.");
            return;
        }

        File file = chooser.getSelectedFile();
        Scanner sc = new Scanner(file);
        
        // Variables for graph handling in main;
        int numberOfGraphs;
        ColoredGraph[] graphs;        
        
        // Check to see if we have # of graphs;
        if (!sc.hasNextInt()) {
            System.out.println("Bad file format. Expecting integer.");
            throw new IOException();
        }
        
        numberOfGraphs = sc.nextInt();
        graphs = new ColoredGraph[numberOfGraphs];
        
        for (int i = 0; i < numberOfGraphs; i++) {
            graphs[i] = ColoredGraph.getGraph(sc);
        }
        
        boolean[][] graph;
        int[] colors;
        boolean success;
        
        for (int j = 0; j < graphs.length; j++) {
            graph = graphs[j].graph;
            
            colors = new int[graph.length];

            if (getColors(graph, colors, 0)) {
                System.out.println(IntStream.of(colors).sum());
                for (int k = 0; k < colors.length; k++) {
                    if (colors[k] == 1) System.out.print((k + 1) + " ");
                }
            };
            
            System.out.print("\n");
            
        }
        
    }
    
    public static boolean getColors(boolean[][] graph, int[] colors, int vertex) {
        if (vertex == graph.length) return true; // Finished the graph;
        
        for (int i = 1; i >= 0; i--) {
            if (!conflict(graph, colors, vertex)) {
                // Set the vertex to black, or 1;
                colors[vertex] = 1;

                // continue looking;
                if (getColors(graph, colors, vertex+1)) return true; 
                
                colors[vertex] = 0;
            } else {
                // Set the vertex to white, or 0;
                colors[vertex] = 0;
                
                // continue looking;
                if (getColors(graph, colors, vertex+1)) return true; 
                
                colors[vertex] = 1;
            }
        }

        return false;
        
    }
    
    // Check if any of it's connections are set to black;
    public static boolean conflict(boolean[][] graph, int[] colors, int vertex) {
        for (int i = 0; i< graph.length; i++) {
            if (graph[vertex][i] && colors[i] == 1) {
                return true;
            }
        }
        return false;
    }
}

class ColoredGraph
{
    // true == black, false == white;
    static boolean[][] graph;

    static ColoredGraph getGraph(Scanner sc) throws IOException
    {
        ColoredGraph g = new ColoredGraph();
        
        // set Variables;
        int numberOfVertices, numberOfEdges;        

        // Make sure we have # of vertices;
        if (!sc.hasNextInt()) {
            System.out.println("Failed to get number of vertices.");
            throw new IOException();
        }
        
        numberOfVertices = sc.nextInt();
        
        // Make sure we have # of edges;
        if (!sc.hasNextInt()) {
            System.out.println("Failed to get number of edges.");
            throw new IOException();
        }
        
        numberOfEdges = sc.nextInt();
        
        // Create a colored nodes adjacency matrix; 
        graph = new boolean[numberOfVertices][numberOfVertices];
                
        for (int i = 0; i < numberOfEdges; i++) {
            // Check to make sure we have an integer;
            if (!sc.hasNextInt()) {
                System.out.println("Failed to get from vertex.");
                throw new IOException();
            }
            int from = sc.nextInt();
            
            // Check to make sure we have an integer again;
            if (!sc.hasNextInt()) {
                System.out.println("Failed to get to vertex.");
                throw new IOException();
            }
            int to = sc.nextInt();
            
            graph[from-1][to-1] = true;
            graph[to-1][from-1] = true;
        }
                
        return g;
    }
    
    static public void error() throws IOException {
        throw new IOException();
    }

}