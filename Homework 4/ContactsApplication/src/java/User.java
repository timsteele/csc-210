/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tim
 */
public class User extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        // Look to see if user sent us 'name' field;
        String name = request.getParameter("user");
                
        // Did the user leave the input empty?
        if ( name != null && !name.equals("") ) { // Did we pass a user name;
            
            // Do we have users in our session?
            ArrayList<String> users = (ArrayList<String>) session.getAttribute("users");
                        
            if ( users != null ) { // We have users in our session;
                
                boolean exists = false;
                
                for ( int i = 0; i < users.size(); i++ ) {
                    if ( users.get( i ).equals(name) ) {
                        exists = true;
                        break;
                    }
                }
                
                // Does use already exist?
                if ( exists ) {
                    setActiveUser( session, name );
                    sendSuccess( response.getWriter(), response, "User '" + name + "' found.");
                }
                else sendError( response.getWriter(), response, "User " + name + " not found. Please register " + name + " as a new user.", 404 );
                
            } else { // No users in our session;
                
                sendError( response.getWriter(), response, "User " + name + " not found. Please register " + name + " as a new user." , 404 );
                
            }
            
        } else { // User left input empty;
            sendError( response.getWriter(), response, "No user name submitted. Please submit a user name.", 400 );
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Establish or get our Session
        HttpSession session = request.getSession();
        
        // Look to see if user sent us 'name' field;
        String name = request.getParameter("user");

        // Did the user leave the input empty?
        if ( name != null && !name.equals("") ) { // Did we pass a user name;
            
            // Do we have users in our session?
            ArrayList<String> users = (ArrayList<String>) session.getAttribute("users");

            if ( users != null ) { // We have users in our session;
                
                boolean exists = false;
                
                for ( int i = 0; i < users.size(); i++ ) {

                    if ( users.get( i ).equals( name ) ) {
                        exists = true;
                        break;
                    }
                }
                
                // Does use already exist?
                if ( exists ) sendError( response.getWriter(), response, "User '" + name + "' already exists. Please register a different name.", 422 );
                else {
                    users.add(name);
                    setActiveUser( session, name );
                    sendSuccess( response.getWriter(), response, "User created." );
                }
                
            } else { // No users in our session;
                
                // Create new users list in our session and add this user;
                ArrayList<String> newUsers = new ArrayList<>();
                newUsers.add( name );
                session.setAttribute( "users", newUsers );
               
                setActiveUser( session, name );
                
                // Okay send good response;
                sendSuccess( response.getWriter(), response, "User created." );
            }   
            
        } else { // User left input empty;
            sendError( response.getWriter(), response, "No user name submitted. Please submit a user name.", 400 );
        }
                
    }
    
    private void sendSuccess( PrintWriter writer, HttpServletResponse response, String message ) {
        response.setContentType( "text/html;charset=UTF-8" );
        writer.println( message );
    }
    
    private void sendError( PrintWriter writer, HttpServletResponse response, String message, int status ) {
        response.setStatus( status );
        response.setContentType( "text/html;charset=UTF-8" );
        
        writer.println( message );
    }
    
    private void setActiveUser( HttpSession session, String name ) {
        session.setAttribute("activeUser", name);
    }
}
