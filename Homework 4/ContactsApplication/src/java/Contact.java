/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tim
 */
public class Contact {
    
    String fName, lName, bdate, phone;
    
    Contact( String fName, String lName, String bdate, String phone ) {
        this.fName = fName;
        this.lName = lName;
        this.bdate = bdate;
        this.phone = phone;
    }

    Contact() {
        this(null, null, null, null);
    }
    
    public void setFName( String name ) {
        this.fName = name;
    }
    
    public void setLName( String name ) {
        this.lName = name;
    }
    
    public void setBirthday( String date ) {
        this.bdate = date;
    }
    
    public void setPhone( String phone ) {
        this.phone = phone;
    }
    
    public String getJSONData() {
        String data = "{"
                + "\"firstName\":\"" + this.fName + "\","
                + "\"lastName\":\"" + this.lName + "\","
                + "\"birthday\":\"" + this.bdate + "\","
                + "\"phone\":\"" + this.phone + "\""
                + "}";
        
        return data;
    }
}
