/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tim
 */
public class Contacts extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        String user = (String) session.getAttribute("activeUser");
        
        // Does the user have contacts?
        HashMap<String, Contact> contacts = (HashMap<String, Contact>) session.getAttribute(user);
        
        if ( contacts != null ) {
            String json = "";
            ArrayList<String> data = new ArrayList<>();
            
            for ( Entry<String, Contact> contact : contacts.entrySet() ) {
                data.add(contact.getValue().getJSONData()); 
            }
            
            for (int i = 0; i < data.size(); i++ ) {
                json += data.get(i);
                if (i < (data.size() - 1)) json += ",";
            }
                        
            sendSuccess( response.getWriter(), response, "[" + json + "]" );
            
        } else {
            sendSuccess( response.getWriter(), response, "[]" );
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean error = false;
        
        // Get our session;
        HttpSession session = request.getSession();
        
        // Get our active user;
        String user = (String) session.getAttribute("activeUser");
        
        // Get our parameters and create our new contact;
        Map<String, String[]> parameters = request.getParameterMap();
        Contact newContact = new Contact();
                
        // Make sure we have all the data we need;
        for ( Entry<String, String[]> entry : parameters.entrySet() ) {
            
            // Make sure we aren't missing anything;
            if ( entry.getValue()[0] == null || entry.getValue()[0].equals("")) {
                error = true;
                sendError( response.getWriter(), response, "{\"" + entry.getKey() + "\": \"Missing value.\"}", 400 );
                break;
            }
            
            // Set values;
            switch( entry.getKey() ) {
                case "firstName":
                    newContact.setFName(entry.getValue()[0]);
                    break;
                case "lastName":
                    newContact.setLName(entry.getValue()[0]);
                    break;
                case "birthday":
                    newContact.setBirthday(entry.getValue()[0]);
                    break;
                case "phone":
                    newContact.setPhone(entry.getValue()[0]);
                    break;
            }
            
        }
           
        if (!error) {
            // Does the user have contacts?
            Map<String, Contact> contacts = (Map<String, Contact>) session.getAttribute(user);

            if ( contacts == null ) { //
                contacts = new HashMap<>();
                session.setAttribute(user, contacts);
            }

            contacts.put( newContact.fName + newContact.lName, newContact );

            sendSuccess( response.getWriter(), response, newContact.getJSONData() );
        }
    }
    
    /**
     * Handles the HTTP <code>Delete</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        HttpSession session = request.getSession();
        
        String user = (String) session.getAttribute("activeUser");
        String id = request.getParameter("id");
        
        HashMap<String, Contact> contacts = (HashMap<String, Contact>) session.getAttribute(user);

        contacts.remove(id);

        sendSuccess( response.getWriter(), response, "User removed" );
        
    }

    private void sendSuccess( PrintWriter writer, HttpServletResponse response, String message ) {
        response.setContentType( "text/html;charset=UTF-8" );
        writer.println( message );
    }
    
    private void sendError( PrintWriter writer, HttpServletResponse response, String message, int status ) {
        response.setStatus( status );
        response.setContentType( "text/html;charset=UTF-8" );
        
        writer.println( message );
    }

}
