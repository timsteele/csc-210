/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tim
 */
public class Details extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        String user = (String) session.getAttribute("activeUser");
        String id = request.getParameter("id");
        
        HashMap<String, Contact> contacts = (HashMap<String, Contact>) session.getAttribute(user);

        String details = contacts.get(id).getJSONData();

        response.setContentType( "text/html;charset=UTF-8" );
        try (PrintWriter writer = response.getWriter() ) {
            writer.println( details );
        }
        
    }


}
