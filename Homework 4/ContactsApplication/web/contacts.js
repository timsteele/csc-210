// Make-shift dirty/simple single page application JS router;
var router = (function () {
    var routes = {
        "/ContactsApplication/": {
            "layout": "home",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database"
            }
        },
        "/ContactsApplication/register": {
            "layout": "user-form",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database",
                "page-subtitle": "Registration Page",
                "user-name-label": "Choose a user name:",
                "button-action": "Register User",
                "form-action": "User",
                "form-method": "POST"
            }
        },
        "/ContactsApplication/login": {
            "layout": "user-form",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database",
                "page-subtitle": "Login Page",
                "user-name-label": "Your User Name:",
                "button-action": "Login",
                "form-action": "User",
                "form-method": "GET"
            }
        },
        "/ContactsApplication/contacts": {
            "layout": "contacts",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database",
                "page-subtitle": "Your List of Contacts"
            },
            "onLoad": function() {
                ajax({
                    url: "/ContactsApplication/Contacts",
                    method: "GET"
                },
                function (response, xhr) {
                    var json = JSON.parse(response);
                                        
                    if (json.length > 0) {
                        var contacts = "",
                            count = json.length;
                        
                        // Contact order doesn't matter, loop backwards it's faster;
                        while (count--) {
                            var current = json[count];
                            contacts += "<tr><td>"+current.firstName+"</td><td>"+current.lastName+"</td><td><button class='btn btn-primary' data-action='details' data-data='" + current.firstName + current.lastName + "'>Details</button></td><td><button class='btn btn-primary' data-action='edit' data-data='" + current.firstName + current.lastName + "'>Edit</button></td><td><button class='btn btn-danger' data-action='delete' data-data='" + current.firstName + current.lastName + "'>Delete</button></td></tr>";
                        }
                        
                        document.getElementById("contactsList").innerHTML = contacts;
                    }
                },
                function (response, xhr) {
                    
                });       
            }
        },
        "/ContactsApplication/contactForm": {
            "layout": "contact-form",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database",
                "page-subtitle": "Enter Data for New Contact",
                "form-method": "POST"
            },
            "onLoad": function () {
                var hash = window.location.hash;
                                
                if (hash) {
                    ajax({
                        url: "/ContactsApplication/Details",
                        method: "GET",
                        data: {
                            id: hash.substr(1)
                        }
                    },
                    function (response, xhr) {
                        var json = JSON.parse(response);
                        
                        for (var id in json) {
                            document.getElementById(id).value = json[id];
                        }

                    },
                    function (response, xhr) { });
                }
            }
        },
        "/ContactsApplication/contactInfo": {
            "layout": "contact-info",
            "data"  : {
                "page-title": "CSC 415-515 Web-base Contacts Database",
                "page-subtitle": "Contact Information"
            },
            "onLoad": function () {
                var hash = window.location.hash;
                
                ajax({
                    url: "/ContactsApplication/Details",
                    method: "GET",
                    data: {
                        id: hash.substr(1)
                    }
                },
                function (response, xhr) {
                    var json = JSON.parse(response),
                        phone = (function (phone) {
                            var phone = phone.replace('-','');
                            
                            return phone.substr(0, 3) + "-" + phone.substr(3,3) + "-" + phone.substr(6);
                        })(json.phone),
                        bdate = (function (bday) {
                            var bday = new Date(bday);
                            
                            return bday.getMonth() + "/" + bday.getDate() + "/" + bday.getFullYear();
                        })(json.birthday)
                    
                    
                            
                    var html = '<h3>' + json.firstName + ' ' + json.lastName + '</h3><p><strong>Phone Number:</strong> ' + phone + '</p><p><strong>Birthday:</strong> ' + bdate + '</p>';
                    
                    document.getElementById("userInfo").innerHTML = html;
                },
                function (response, xhr) {
                    
                });       
            }
        }
    };
    return function ( route, addHistory, extraData, replace ) {
                
        // Get our app routing data;
        routing = this[route.replace(/(#[a-zA-Z]+)$/,'')];
        
        // Are we adding this to history?
        if ( addHistory ) {
            if (replace) {
                window.history.replaceState({
                    'route' : route
                }, "", route);
            } else {
                window.history.pushState({
                    'route' : route
                }, "", route);
            }
        }

        var data = (extraData) ? Object.assign( {}, routing.data, extraData ): routing.data;

        // Set out template;
        document.getElementById("main").innerHTML = templates( routing.layout, data );
        
        if ( routing.onLoad ) routing.onLoad();
        
    }.bind( routes );
})();

/**
 * @desc set up template handler
 */
var templates = (function () {

    // Normally you would compile these, but for simplicity
    // I am going to run them as functions for readability;
    // Set all of our page templates here;
    var templates = {
        "home": (function () {
            return  '<h1>{{page-title}}</h1>' +
                    '<p>' +
                        '<a class="btn btn-primary" href="register">Register</a> or <a class="btn btn-primary" href="login">Login</a> ' +
                    '</p>';
        })(),
        "user-form": (function () {
            return  '<h1>{{page-title}}</h1>' +
                    '<h2>{{page-subtitle}}</h2>' +
                    '<form action="{{form-action}}" method="{{form-method}}" id="userForm">' +
                        '<div class="form-group">' +
                            '<label for="userName">{{user-name-label}}</label>' +
                            '<input type="text" name="userName" id="userName" class="form-control" placeholder="User Name" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<button type="submit" class="btn btn-primary">{{button-action}}</button>' +
                        '</div>' +
                    '</form>';
        })(),
        "contacts": (function () {
            return  '<h1>{{page-title}}</h1>' +
                    '<h2>{{page-subtitle}}</h2>' +
                    '<table clas="table" style="width:50%;">' +
                        '<thead>' +
                            '<tr>' +
                                '<th style="width:50%;">First Name</th>' + 
                                '<th style="width:50%;">Last Name</th>' +
                                '<th>&nbsp;</th>' +
                                '<th>&nbsp;</th>' +
                                '<th>&nbsp;</th>' +
                            '</tr>' +
                        '</thead>' +
                        '<tfoot>' +
                            '<tr>' +
                                '<td colspan="5"><a class="btn btn-primary" href="contactForm"><i class="fa fa-plus-circle"></i> Add New Contact </button></td>' +
                            '</tr>' +
                        '</tfoot>' +
                        '<tbody id="contactsList">' +
                            '<tr>' +
                                '<td colspan="5">You do not have any contacts.</td>' +
                            '</tr>' +
                        '</tbody>' +
                    '</table>';
        })(),
        "contact-form": (function () {
            return  '<h1>{{page-title}}</h1>' +
                    '<h2>{{page-subtitle}}</h2>' +
                    '<form action="Contacts" method="{{form-method}}" id="contactForm" style="width:50%;">' +
                        '<div class="form-group">' +
                            '<label for="firstName">First Name</label>' +
                            '<input type="text" name="firstName" id="firstName" class="form-control" placeholder="First Name" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label for="lastName">Last Name</label>' +
                            '<input type="text" name="lastName" id="lastName" class="form-control" placeholder="Last Name" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label for="birthday">Birthday</label>' +
                            '<input type="date" name="birthday" id="birthday" class="form-control" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label for="phone">Phone Number</label>' +
                            '<input type="tel" name="phone" id="phone" class="form-control" placeholder="Format: 123-456-7890" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<button type="submit" class="btn btn-primary">Submit</button>' +
                        '</div>' +
                    '</form>';
        })(),
        "contact-info": (function () {
            return  '<h1>{{page-title}}</h1>' +
                    '<h2>{{page-subtitle}}</h2>' +
                    '{{success-message}}' +
                    '<div id="userInfo"></div>' +
                    '<p><a href="contacts" class="btn btn-primary">Back to My Contacts</button></p>';
        })()
    };

    // This is what is actually being set to templates;
    /**
     * @desc merges templates and template data together and returns final HTML
     * 
     * @param {Object} templates - the application templates (bound);
     * @param {String} templateName - the name of the template to be used;
     * @param {Object} data - data for the template data placeholders;
     */
    return function ( templates, templateName, data ) {
        
        // Return a string with all {{}} replace with
        // corresponding `data` property values;
        return (function ( template, data ) {
            
            // Replaceing {{}} with `data` properties;
            return template.replace(/{{([a-zA-Z-\.]+)}}/g, function(match, substring, index, original) { 
                return (data[substring]) ? data[substring]: '';
            });

        }).call( this, templates[templateName], data );

    }.bind( this, templates );
})();

// Dirty/simple Ajax requester
var ajax = function ajax( configs, onSuccess, onError ) {
    
    var xhr = new XMLHttpRequest();
    
    // Handle data return;
    xhr.onreadystatechange = function() {
        
        // Is the request finished?
        if ( xhr.readyState === 4 ) {
            
            if ( xhr.status === 200 ) { // Success
                onSuccess( xhr.responseText, xhr )
            } else { // Failure
                onError( xhr.responseText, xhr )
            }
            
        }
    }
                
    if ( configs.method.toLowerCase() === "get" || configs.method.toLowerCase() === "delete" ) {
        if ( configs.data ) {
            var data  = configs.data, // Cache ref;
                query = [];
            
            for (var key in data) {
                query.push( encodeURIComponent(key) + "=" +  encodeURIComponent(data[key]));
            }
            
            configs.url = configs.url + (query.length ? '?' + query.join('&'):'');
        }
                
        xhr.open( configs.method, configs.url );
        xhr.send();
    } else {
        if ( configs.data ) {
            var data  = configs.data, // Cache ref;
                query = [];
            
            for (var key in data) {
                query.push( encodeURIComponent(key) + "=" +  encodeURIComponent(data[key]));
            }
        }
                
        xhr.open( configs.method, configs.url );
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send( query.join('&') );
    }

};

// namespace for funcionality specific to the app;
databaseApp = new function () {
    
    this.formHandlers = {
        "userForm": function ( event ) {
            event.preventDefault(); // Prevent normal submit;
                   
            var form = event.target;

            ajax({
                     url: form.action,
                     method: form.getAttribute("method"),
                     data: {
                         user: document.getElementById("userName").value
                     }
                 }, 
                 function( response, xhr ) {
                    router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/contacts'), true );
                 }, 
                 function( response, xhr ) {
                    function errorHandler( response ) {
                        var fieldWrapper = document.getElementById("userName").parentNode;
                        
                        if ( fieldWrapper.className.indexOf('has-error') + 1 ) { // Field already in error;
                            
                            fieldWrapper.getElementsByTagName('span').item(0).innerHTML = response;
                            
                        } else {
                            
                            fieldWrapper.className += ' has-error';
                        
                            var errorMessage = document.createElement("span");
                                errorMessage.className = 'help-block';
                                errorMessage.appendChild( document.createTextNode(response) );

                            fieldWrapper.appendChild(errorMessage);
                        }
                    };
                    
                    if ( xhr.status !== 404 ) {
                        errorHandler( response );
                    } else {
                        router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/register'), true );
                        errorHandler( response );
                    }
                 }
             );
        }.bind( this ),
        "contactForm": function ( event ) {
            event.preventDefault(); // Prevent normal submit;
                   
            var form = event.target;

            ajax({
                     url: form.action,
                     method: form.getAttribute("method"),
                     data: {
                        firstName: document.getElementById("firstName").value,
                        lastName: document.getElementById("lastName").value,
                        birthday: document.getElementById("birthday").value,
                        phone: document.getElementById("phone").value
                     }
                 }, 
                 function( response, xhr ) {
                    var data = JSON.parse(response);
                    router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/contactInfo#'+data.firstName+data.lastName), true );
                 }, 
                 function( response, xhr ) {
                    var json = JSON.parse(response);
                    
                    for (var field in json) {
                        var fieldWrapper = document.getElementById(field).parentNode;

                        if ( fieldWrapper.className.indexOf('has-error') + 1 ) { // Field already in error;

                            fieldWrapper.getElementsByTagName('span').item(0).innerHTML = json[field];

                        } else {

                            fieldWrapper.className += ' has-error';

                            var errorMessage = document.createElement("span");
                                errorMessage.className = 'help-block';
                                errorMessage.appendChild( document.createTextNode(json[field]) );

                            fieldWrapper.appendChild(errorMessage);
                        }
                    }
                     
                    
                 }
             );
        }.bind( this )
    };
    
    this.actions = {
        "delete": function ( id, element ) {
            
            ajax({
                url: "/ContactsApplication/Contacts",
                method: "DELETE",
                data: {
                    id: id
                }
            },
            function (target, response, xhr) {
                router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/contacts'), true, null, true );
            },
            function (response, xhr) {

            });
            
        }.bind( this ),
        "edit": function ( id ) {
            router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/contactForm#' + id), true );
        }.bind( this ),
        "details": function ( id ) {
            router( (window.location.pathname).replace(/(\/[a-zA-Z]+)$/, '/contactInfo#' + id), true );
        }.bind( this )
    };
    
    return this;  
};

// Activate the router/routing;
// and event delegation at the root;
window.addEventListener("DOMContentLoaded", function() {
    
    debugger;
    
    // START ROUTER
    // Load the template on entry
    var pathname = window.location.pathname,
        routing, content;
        
    router(pathname);
    
    // LISTER FOR NAVIGATION EVENTS
    window.addEventListener("click", function( event ) {
       
        // Cache target;
        var target = event.target;
        
        if (target.nodeName === "A") {
            // replace link navigation;
            event.preventDefault();
                        
            router(target.pathname, true);
            
        }
        
        if (target.nodeName === "BUTTON") {
            var action;
            
            if (action = target.getAttribute("data-action")) {
                databaseApp.actions[action]( target.getAttribute("data-data"), target );
            }
        
        }
        
    });
    
    // Adding form handling delegation to the window
    // so I don't have to add/remove listeners between
    // pages;
    
    window.addEventListener('submit', function( event ) {
       var target = event.target;
       
       if (target.nodeName === "FORM") {
           event.preventDefault();
           
           databaseApp.formHandlers[target.id](event)
       }
    });
});

// Listen for navigation events
window.addEventListener('popstate', function ( event ) {
    // Get push history pop state
    var pathname = window.location.pathname;
    
    // Grab the pathname for the router;
    if (pathname.charAt(pathname.length - 1) === "/") pathname = pathname.substr(0, pathname.length-1);

    // commence routing;
    router(pathname);
});

