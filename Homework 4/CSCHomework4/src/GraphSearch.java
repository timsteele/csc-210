
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tim
 */
public class GraphSearch
{    
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "Graph Data", "txt", "grf");

        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(null);

        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
            System.out.println("No file selected.");
            return;
        }

        File file = chooser.getSelectedFile();
        Scanner sc = new Scanner(file);
        GraphRepresentation graph = GraphRepresentation.getGraph(sc);
        
        System.out.println(graph);
        
        String[] vertices = graph.vertexNames;
        int[] pred;
        
        pred = new int[vertices.length];
        graph.BFS(graph, 0, pred);
        
        List<Integer> path;
        
        System.out.println("Breadth First Paths are:");
        for (int i = 0; i<vertices.length; i++) {
            path = new LinkedList<>();
            System.out.println("Path to the vertex v " + i + " | " + graph.vertexNames[i]);
            
            graph.getPath(graph, i, pred, (LinkedList<Integer>) path);
            
            System.out.println(path.toString());
            System.out.print("[");
            
            for (int k = 0; k < path.size(); k++) {
                System.out.print(graph.vertexNames[path.get(k)]);
                if (k != (path.size() - 1)) System.out.print(", ");
            }
            
            System.out.println("]");
        }
        
    }
}

class GraphRepresentation
{
    // fields
    static String[] vertexNames;
    static List<List<Integer>> adjL;
    static Map<String, Integer> strToIntMap;

    static GraphRepresentation getGraph(Scanner sc) throws IOException
    {
        GraphRepresentation g = new GraphRepresentation();
        
        // Get # of vertices;
        int numberOfVertices = 0;
        if (!sc.hasNextInt()) {
            System.out.println("Bad file format. Expecting integer.");
            throw new IOException();
        }
        
        numberOfVertices = sc.nextInt();    
                    
        // Get vertices;
        if (!sc.hasNext()) {
            System.out.println("Bad file format. Expecting line.");
            throw new IOException();
        }
        
        vertexNames = new String[numberOfVertices];
        strToIntMap = new HashMap<>();
        adjL = new ArrayList<>();
                
        for (int i=0; i<numberOfVertices; i++){
            if (sc.hasNext()) {
                String name = sc.next(); 
                vertexNames[i] = name;
                strToIntMap.put(name, i);
                adjL.add(new ArrayList<>());
            }
        }
        
        // Init. this once;
        String[] vertex;
                
        for (int j=0; j<numberOfVertices;j++) {
            int vertexMappedInt = -1, mappedIndex, numberOfOutDeg = 0;
                    
            // Make sure we have a next line;
            if (!sc.hasNext()) {
                error();
            }
                                    
            if (sc.hasNext()) vertexMappedInt = strToIntMap.get(sc.next());
            else error();
            
            if (sc.hasNext()) numberOfOutDeg = Integer.parseInt(sc.next());
            else error();
            
            int relationshipWalker = 0;
            
            while (relationshipWalker < numberOfOutDeg) {
                if (sc.hasNext() || vertexMappedInt != -1) {
                    mappedIndex = strToIntMap.get(sc.next());
                    adjL.get(vertexMappedInt).add(mappedIndex);
                    relationshipWalker++;
                } else error();

            }     
        }    

        
        
        return g;
    }
    
    static public void error() throws IOException {
        throw new IOException();
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        
        builder.append(Arrays.toString(vertexNames)).append("\n");
        builder.append(strToIntMap.toString()).append("\n\n");
        
        int stepper = 0;
        while (stepper < adjL.size()) {
            builder.append(stepper + " : " + adjL.get(stepper).toString()).append("\n");
            stepper++;
        }
        
        builder.append("\n");
        stepper = 0;
        
        while (stepper < adjL.size()) {
            builder.append(vertexNames[stepper] + " : ");
            
            int relationships = 0;
            builder.append("[ ");
            
            while (relationships < adjL.get(stepper).size()) {
                if (relationships != 0) builder.append(", ");
                builder.append(vertexNames[adjL.get(stepper).get(relationships)]);
                relationships++;
            }
            
            builder.append(" ]\n");
            stepper++;
        }
        
        return builder.toString();
    }
    
    static void BFS(GraphRepresentation g, int v, int[] pred) {
        Queue<Integer> queue = new LinkedList<>();
        List<Integer> visitedVertices = new LinkedList<>();
        
        pred[v] = -1; 
        queue.add(v);
        visitedVertices.add(v);
                
        while(!(queue.isEmpty())) {
            int vertex = queue.remove();
            for (int i = 0; i < g.adjL.get(vertex).size(); i++) {
                if (!visited(visitedVertices, g.adjL.get(vertex).get(i))) {
                    pred[g.adjL.get(vertex).get(i)] = vertex;
                    visitedVertices.add(g.adjL.get(vertex).get(i));
                    queue.add(g.adjL.get(vertex).get(i));
                }
            }
        }        
        
    }
    
    static void getPath(GraphRepresentation g, int v, int [] pred, LinkedList<Integer> path) {
        int prior = pred[v],
            index = v;
        
        while(prior != -1) {
            path.add(0, index);
            index = prior;
            prior = pred[prior];
        }
        
        path.add(0, index);
    }
    
    static boolean visited(List<Integer> array, int vertex) {
        for (int i = 0; i < array.size(); i++) {
            if (vertex == array.get(i)) return true;
        }
        
        return false;
    }
    
}