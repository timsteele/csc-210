// Prevent Global Leaks
(function () {

    // App data;
    var App = new function() {

        this.container = document.getElementById("main");

    };

    // Make-shift dirty/simple single page application JS router;
    var router = (function () {
        var routes = {
            "/CarSeller": "Home",
            "/CarSeller/Admin": "Admin",
            "/CarSeller/Shop": "Shop"
        };

        return function ( route, ignoreHistory, replace ) {

            // Get our app routing data;
            // Remove hashes and trailing slashes;
            // Probably should be a regex....;
            routing = this[ route.replace( /\/?(#[a-zA-Z]+)?$/, '' ) ];

            // Are we adding this to history and/or replacing
            // a place in history.
            if ( !ignoreHistory ) {
                if (replace) {
                    window.history.replaceState({
                        'route' : route
                    }, "", route);
                } else {
                    window.history.pushState({
                        'route' : route
                    }, "", route);
                }
            }

            Controllers.get( routing ).show();

        }.bind( routes );
    })();

    // Create a master controller to hold my
    // route controllers;
    var Controllers = new function () {

        var controllers = {},
            Controller = function(properties) {

                // Assign any passed properties to controller
                for ( var prop in properties ) {
                    this[prop] = properties[prop];
                }

                // If it has an init, invoke it;
                if ( this.init ) this.init();

                // Show the controller;
                this.show = function() {

                    App.container.innerHTML = this.template;

                    if ( this.onShow ) this.onShow();
                }

                return this;
            }

        // Return a controller;
        this.get = function ( controller ) {
            if ( controllers[controller] ) return controllers[ controller ];
            else return false;
        }

        // Register a new controller;
        this.register = function ( name, options ) {
            if ( !controllers[ name ] ) {
                controllers[ name ] = new Controller( options );

                // return true - controller registered;
                return true;
            } else {

                // naming conflict, controller not registered;
                return false;

            }
        }
    };

    // Manage application views (separate from controllers);
    var Views = new function() {
        var views = {},
            View = function(properties) {

                // Private function, sets events after
                // rendering;
                var setEvents = function() {
                    // Set the events on the object;
                    if ( this.events ) {
                        for ( var selector in this.events ) ( function ( selector, eventData ) {

                            var elements = this.region.querySelectorAll( selector ),
                                count;

                            if ( (count = elements.length) ) {

                                while ( count-- ) {
                                    elements[ count ].addEventListener( eventData.listener, eventData.callback.bind( this ) );

                                }

                            }

                        }.call( this, selector, this.events[selector] ) );
                    }
                }

                // This is what is merging templates with data;
                this.render = function ( template, data ) {

                    // Return a string with all {{}} replace with
                    // corresponding `data` property values;
                    return (function renderData( template, data ) {

                        // Replaceing {{}} with `data` properties;
                        return template.replace(/{{([a-zA-Z-\.]+)}}/g, function( match, substring, index, original ) {
                            return (data[substring]) ? data[substring]: '';
                        });

                    }).call( this, template, data );

                }.bind( this );

                // If we have default values, set 'em;
                if ( properties[ 'defaults' ] ) this.data = properties[ 'defaults' ];

                // Replace default values with data passed
                // in to the template;
                this.setData = (function ( data ) {

                    function evaluate( data ) {
                        if ( data ) {
                            for ( var d in data ) {
                                this.data[ d ] = data[ d ];
                            }
                        }
                    }

                    evaluate.call( this, data );

                    return evaluate;
                }).call( this, properties['data'] );

                // Any properties passed in, set on the
                // the object;
                for ( var prop in properties ) {

                    this[prop] = properties[prop];
                }

                if (this.init) this.init();

                this.show = function( region ) {

                    if ( region ) this.region = document.getElementById( region );
                    this.region.innerHTML = this.render( this.template, this.data );

                    // Set any events;
                    setEvents.call( this );

                    // If there is an onShow method;
                    if ( this.onShow ) this.onShow();
                }

                return this;
            }

        // Return a view;
        this.get = function ( view ) {
            if ( views[view] ) return views[ view ];
            else return false;
        }

        // Register a view;
        this.register = function ( name, options ) {
            if ( !views[ name ] ) {
                views[ name ] = new View( options );

                // return true - controller registered;
                return true;
            } else {

                // naming conflict, controller not registered;
                return false;
            }
        }
    }


    // Register the Admin view;
    // Set configuration for it;
    Views.register(
        'Admin',
        {
            events: { // Set an view specific events here, will be set on view render;
                'form': {
                    listener: 'submit',
                    callback: function ( event ) {
                        event.preventDefault();

                        // Create form data;
                        var form = event.target,
                            data = new FormData();

                        // Form File Fields;
                        var imageField = document.getElementById( 'image' ),
                            longDescriptionField = document.getElementById( 'longDescription' );

                        // package form data fro Ajax request;
                        data.append( 'shortDescription', document.getElementById( 'shortDescription' ).value );
                        data.append( imageField.name, imageField.files[0], imageField.files[0].name );
                        data.append( longDescriptionField.name, longDescriptionField.files[0], longDescriptionField.files[0].name );

                        ajax(
                            {
                                url: form.action,
                                method: form.method,
                                contentType: form.enctype,
                                formData: data
                            },
                            function ( response, xhr ) {
                                // After we are done, trigger the addCar event;
                                // Anything listening can update;
                                document.dispatchEvent( new CustomEvent( 'addCar', { detail: JSON.parse(response) } ) );
                            },
                            function ( response, xhr ) {
                                alert( "Failed to add new car." );
                            }
                        );
                    }
                }
            },
            template:   '<h2>Add A Car</h2>' +
                        '<form method="POST" action="/CarSeller/Car" enctype="multipart/form-data">' +
                            '<div class="form-group">' +
                                '<label for="shortDescription">Short Description</label>' +
                                '<input type="text" name="shortDescription" id="shortDescription" class="form-control" placeholder="Short Description" />' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label for="image">Image</label>' +
                                '<input type="file" name="image" id="image" class="form-control" />' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label for="longDescription">Long Description</label>' +
                                '<input type="file" name="longDescription" id="longDescription" class="form-control" />' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<button type="submit" class="btn btn-primary">Submit</button>' +
                            '</div>' +
                        '</form>'
        }
    );

    // Register User view;
    Views.register(
        'User',
        {
            defaults: { // Any default data;
                image:'',
                longDescription: '',
                shortDescription: ''
            },
            init: function ( event ) {
                document.addEventListener( 'addCar', function( event ) {
                    // Capture any cars that are added to the document;
                    var newCar = event.detail,
                        option = document.createElement( 'option' );

                    option.setAttribute( 'value', newCar.id );
                    option.appendChild( document.createTextNode( newCar.description ) );

                    document.getElementById( 'carChooser' ).appendChild( option );

                }.bind( this ) );

            },
            onShow: function () {

                // This gets cars on the loading of the user chooser view;
                ajax(
                    {
                        url: '/CarSeller/Cars',
                        method: 'GET'
                    },
                    function ( response, xhr ) {
                        // On successful response;

                        var cars = JSON.parse( response ),
                            fragment = document.createDocumentFragment(),
                            count = cars.length;

                        // Loop throgh our car objects;
                        // Create option elements;
                        while ( count-- ) {
                            var option = document.createElement( 'option' );

                            option.setAttribute( 'value', cars[count].id );
                            option.appendChild( document.createTextNode( cars[count].description ) );

                            fragment.appendChild( option );
                        }

                        // Attach to select element;
                        document.getElementById( 'carChooser' ).appendChild( fragment );
                    },
                    function ( response, xhr ) {
                        alert( "Failed to retrieve cars." );
                    }
                );

                // Listen for changes to the select menu;
                document.getElementById( 'carChooser' ).addEventListener( 'change', function ( event ) {

                    // Get the current value of the select menu;
                    var target = event.target,
                        value = target.options[ target.selectedIndex ].value;

                    // If not the default value, go and get a car;
                    if ( value !== "default" ) {
                        ajax(
                            {
                                url: '/CarSeller/Car',
                                method: 'GET',
                                data: {
                                    id:value
                                }
                            },
                            function ( response, xhr ) {

                                // if this was an actual full blown web app
                                // I would have sub-view support so we wouldn't
                                // do a total view re-render (a.k.a. not re-render select menu);
                                this.data = JSON.parse( response );
                                this.show();

                                var carDetails = document.getElementById('carDetails');

                                carDetails.className = carDetails.className.replace( / ?hidden/, '' );


                            }.bind( this ),
                            function ( response, xhr ) {
                                alert( 'Failed to get car.' );
                            }
                        )
                    }

                }.bind( this ) );
            },
            template:   '<h2>View A Car</h2>' +
                        '<section>' +
                            '<div class="form-group">' +
                                '<label for="carChooser">Short Description</label>' +
                                '<select name="carChooser" id="carChooser" class="form-control" />' +
                                    '<option value="default">Select Car</option>' +
                                '</select>' +
                            '</div>' +
                            '<section id="carDetails" class="row hidden">' +
                                '<section class="col-sm-3">' +
                                        '<img id="carImage" src="{{image}}" />' +
                                '</section>' +
                                '<section class="col-sm-9">' +
                                    '<h3> Short Description </h3>' +
                                    '<p> {{shortDescription}} </p>' +
                                    '<h3> Long Description </h3>' +
                                    '<p> {{longDescription}} </p>' +
                                '</section>' +
                            '</section>' +
                        '</section>'

        }
    );

    // Register Home Page
    Controllers.register( 'Home', {
        template: '<h1>North Central Car Dealership</h1>'
    });

    // Register Admin
    Controllers.register( 'Admin', {
        template:   '<h1>Administrator Page</h1>' +
                    '<section id="addCar"></section>' +
                    '<hr />' +
                    '<section id="viewCar"></section>',
        regions: {
            'admin': 'addCar',
            'user': 'viewCar'
        },

        onShow: function () {
            Views.get( 'Admin' ).show( this.regions.admin );
            Views.get( 'User' ).show( this.regions.user );
        }
    });

    // Register Shop
    Controllers.register( 'Shop', {
        template:  '<h1>Car Inventory</h1>' +
                    '<section id="viewCar"></section>',
        regions: {
            'user': 'viewCar'
        },

        onShow: function () {
            Views.get( 'User' ).show( this.regions.user );
        }
    });

    // Dirty/simple Ajax requester
    var ajax = function ajax( configs, onSuccess, onError ) {

        var xhr = new XMLHttpRequest();

        // Handle data return;
        xhr.onreadystatechange = function() {

            // Is the request finished?
            if ( xhr.readyState === 4 ) {

                if ( xhr.status === 200 ) { // Success
                    if (onSuccess) onSuccess( xhr.responseText, xhr )
                } else { // Failure
                    if (onError) onError( xhr.responseText, xhr )
                }

            }
        }

        if ( configs.method.toLowerCase() === "get" || configs.method.toLowerCase() === "delete" ) {
            if ( configs.data ) {
                var data  = configs.data, // Cache ref;
                    query = [];

                for (var key in data) {
                    query.push( encodeURIComponent(key) + "=" +  encodeURIComponent(data[key]));
                }

                configs.url = configs.url + (query.length ? '?' + query.join('&'):'');
            }

            xhr.open( configs.method, configs.url );
            xhr.send();
        } else {
            if ( configs.data ) {
                var data  = configs.data, // Cache ref;
                    query = [];

                for (var key in data) {
                    query.push( encodeURIComponent(key) + "=" +  encodeURIComponent(data[key]));
                }
            }

            xhr.open( configs.method, configs.url );
            // There is a weird bug in the browser with form data;
            // If you manually set it to multipart-form, it doesn't include the
            // boundaries and blows up glassfish. If you leave it blank, the
            // browser figures it out for you;
            // xhr.setRequestHeader('Content-type', ( configs.contentType )? undefined:'application/x-www-form-urlencoded');
            if ( configs.formData ) xhr.send( configs.formData );
            else xhr.send( (query)? query.join('&'):'' );
        }

    };

    // Activate the router/routing;
    // and event delegation at the root;
    window.addEventListener("DOMContentLoaded", function() {

        // START ROUTER
        // Load the template on entry
        var pathname = window.location.pathname,
            routing, content;

        router(pathname, true);

        // LISTER FOR NAVIGATION EVENTS
        window.addEventListener("click", function( event ) {

            // Cache target;
            var target = event.target;

            if (target.nodeName === "A") {
                // replace link navigation;
                event.preventDefault();

                router(target.pathname);

            }

        });

    });

    // Listen for navigation events
    window.addEventListener('popstate', function ( event ) {
        // Get push history pop state
        var pathname = window.location.pathname;

        // Grab the pathname for the router;
        if (pathname.charAt(pathname.length - 1) === "/") pathname = pathname.substr(0, pathname.length-1);

        // commence routing;
        router(pathname);
    });


})();
