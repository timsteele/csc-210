/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tim
 */
public class Cars extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext sc = request.getServletContext();
        
        // Get our cars;
        HashMap cars = (HashMap) sc.getAttribute("cars");

        // For building our response;
        StringBuilder json = new StringBuilder();
        int rCount = 0;
        
        // Store our json objects;
        String[] jsonStrings = new String[cars.size()];
        int counter = 0;
        
        // Get values;
        for (Iterator it = cars.values().iterator(); it.hasNext();) {
            CarInfo car = (CarInfo) it.next();
            jsonStrings[counter] = "{\"id\":\"" + car.carName + "\", \"description\": \"" + car.shortDescription + "\"}";
            counter++;
        }
        
         // Start building our JSON response;
        
        json.append("[");
        
        while ( rCount < jsonStrings.length ) {
            
            json.append( jsonStrings[rCount] );
            
            rCount++;
            if (rCount != jsonStrings.length) json.append(",");
            
        } 
        
        json.append("]");
        
        String jsonResponse = json.toString();
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println(jsonResponse);
        }
        
    }

}
