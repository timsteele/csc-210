/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Tim
 */
@WebServlet(urlPatterns = {"/Car"})
@MultipartConfig
public class Car extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext sc = request.getServletContext();
        
        // Get our data;
        File web = (File) sc.getAttribute("web");
        HashMap<String, CarInfo> cars = (HashMap) sc.getAttribute("cars");

        String id = request.getParameter("id");
        
        // Get our car;
        CarInfo car = (CarInfo) cars.get(id);
        
        // Simple data;
        String name = car.carName;
        String shortDescription = car.shortDescription;
        
        // Build our text file/long description;
        StringBuilder lDescript = new StringBuilder();
        File textFile = new File(web, "WEB-INF/" + name + ".txt" );
        
        
        FileReader fileReader = new FileReader(textFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        
        while( (line = bufferedReader.readLine()) != null ) {
            lDescript.append(line);
        }
        
        bufferedReader.close();
        
        String longDescription = lDescript.toString();
        
        // Get our image;
        File dir = new File(web, "images");

        File[] matches = dir.listFiles( new FilenameFilter() {
            @Override
            public boolean accept(File dir, String image) {
                return image.startsWith(name);
            }
        });
        
        String image = matches[0].getName();
        
        // Building our response;
        StringBuilder json = new StringBuilder();
        json.append("{");
        
        json.append("\"id\": \"" + name + "\",");
        json.append("\"shortDescription\": \"" + shortDescription + "\",");
        json.append("\"longDescription\": \"" + longDescription.replace("\"", "&quot;") + "\",");
        json.append("\"image\": \"http://localhost:8080/CarSeller/images/" + image + "\"");
                
        json.append("}");
        
        String jsonResponse = json.toString();
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println(jsonResponse);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext sc = request.getServletContext();
        
        // Get our web folder;
        File web = (File) sc.getAttribute("web");
        HashMap cars = (HashMap) sc.getAttribute("cars");
        
        // Generate a unique id for each car;
        String uniqueID = UUID.randomUUID().toString();
        
        // Get our form data;
        Part carImage = request.getPart("image");
        Part longDescription = request.getPart("longDescription");
        
        // Get image extension;
        String imageName = carImage.getSubmittedFileName();
        int i = imageName.lastIndexOf('.');
        String imageExt = imageName.substring(i + 1);
                
        // Save image
        InputStream imageContent = carImage.getInputStream();
        File imageFile = new File(web, "images/" + uniqueID + "." + imageExt);
        Files.copy(imageContent, imageFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        // Save text
        InputStream textContent = longDescription.getInputStream();
        File textFile = new File(web, "WEB-INF/" + uniqueID + ".txt");
        Files.copy(textContent, textFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        // Add car to our map;
        // Glassfish issue -- if you try and get the Parameter for the Parts
        // It blows up Glassfish.
        String shortDescription = request.getParameter("shortDescription");
        cars.put(uniqueID, new CarInfo(uniqueID, shortDescription) );
        
        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
            out.println("{\"id\": \"" + uniqueID + "\", \"description\":\"" + shortDescription + "\"}" );
        }
    }

}
