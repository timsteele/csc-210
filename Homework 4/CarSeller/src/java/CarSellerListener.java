
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Web application lifecycle listener.
 *
 * @author Tim
 */
public class CarSellerListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        // Get the context;
        ServletContext sc = sce.getServletContext();
        
        // Get our real path to set our File paths;
        String realPath = sc.getRealPath("/");
        
        // Get the web path;
        File web = new File( new File(realPath).getParentFile().getParentFile(), "web" );
        
        Map<String, CarInfo> cars = new HashMap<>();
        
        sc.setAttribute("cars", cars);
        sc.setAttribute("web", web);
        //sc.setAttribute("images", new File(web, "images"));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {}
}
